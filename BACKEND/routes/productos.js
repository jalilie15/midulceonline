// se va asignar las rutas del modulo producto

const express=require('express');
const router=express.Router();

const productoController=require('../controllers/productoController');

//Rutas CRUD
router.get('/',productoController.consultarProductos);
router.post('/',productoController.agregarProductos);
router.get('/:id',productoController.obtenerProducto);
router.delete('/:id',productoController.eliminarProducto);
router.put('/:id',productoController.actualizarProducto);







module.exports=router;
