// se va asignar las rutas del modulo producto

const express=require('express');
const router=express.Router();

const proveedoresController=require('../controllers/proveedoresController');

//Rutas CRUD MODULO PRODUCTO
router.get('/',proveedoresController.consultarProveedores);
router.post('/',proveedoresController.agregarProveedores);
router.get('/:id',proveedoresController.obtenerProveedores);
router.delete('/:id',proveedoresController.eliminarProveedores);
router.put('/:id',proveedoresController.actualizarProveedores);

// Rutas CRUD MODULO PROVEEDORES








module.exports=router;