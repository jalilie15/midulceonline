// se va asignar las rutas del modulo producto

const express=require('express');
const router=express.Router();

const clienteController=require('../controllers/clienteController');

//Rutas CRUD
router.get('/',clienteController.consultarCliente);
router.post('/',clienteController.agregarCliente);
router.get('/:id',clienteController.obtenerCliente);
router.delete('/:id',clienteController.eliminarCliente);
router.put('/:id',clienteController.actualizarCliente);







module.exports=router;