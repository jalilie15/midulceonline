const Producto = require("../models/Producto");
// CONSULTAR PRODUCTO POR JSON
exports.consultarProductos = async(req,res)=>{

    try{
        const producto=await Producto.find();
        res.json(producto)
    }catch(error){
        console.log(error);
        res.status(500).send('hubo un error al consultar los datos');
    }
}
// AGREGAR PRODUCTO POR JSON
exports.agregarProductos=async(req,res) => {
    try{
        let producto;
        producto=new Producto(req.body);

        await producto.save();
        res.send(producto);

    }catch (error) {
        console.log(error);
        res.status(500).send("no se puede guardar el producto");

    }
}
// OBTENER PRODUCTO POR ID DESDE EL JSON
exports.obtenerProducto = async (req,res) => {
    try{
        let producto=await Producto.findById(req.params.id);

        if (!producto){
            res.status(404).json({msg:'no se puede encontrar ese producto'})
        }
        res.send(producto);

    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");

    }
}
// Eliminar producto

exports.eliminarProducto = async (req,res) =>{
    try{
        let producto=await Producto.findById(req.params.id);

        if (!producto){
            res.status(404).json({msg:'no existe producto'})
        }
        await Producto.findOneAndRemove({_id: req.params.id });
        res.json({msg: 'el producto está eliminado'});

    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");

    }
}
// ACTUALIZAR PRODUCTO
exports.actualizarProducto = async (req,res) => {
    try{
        const {nombre, categoria, precio, presentacion, cantidad, precio_venta} =req.body;
        let producto = await Producto.findById(req.params.id);
        if (!producto){
            res.status(404).json({msg: 'el producto no existe'});

        }
        producto.nombre= nombre;
        producto.categoria = categoria;
        producto.precio = precio;
        producto.presentacion = presentacion;
        producto.cantidad = cantidad;
        producto.precio_venta = precio_venta;
        producto = await Producto.findOneAndUpdate({_id: req.params.id}, producto,{new:true});
        res.json(producto);
    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");
    }
}
