const Cliente = require("../models/Cliente");
// CONSULTAR PRODUCTO POR JSON
exports.consultarCliente = async(req,res)=>{

    try{
        const cliente=await Cliente.find();
        res.json(cliente)
    }catch(error){
        console.log(error);
        res.status(500).send('hubo un error al consultar los clientes');
    }
}
// AGREGAR CLIENTE POR JSON
exports.agregarCliente=async(req,res) => {
    try{
        let cliente;
        cliente=new Cliente(req.body);

        await cliente.save();
        res.send(cliente);

    }catch (error) {
        console.log(error);
        res.status(500).send("no se puede guardar el cliente");

    }
}
// OBTENER CLIENTE POR ID DESDE EL JSON
exports.obtenerCliente = async (req,res) => {
    try{
        let cliente=await Cliente.findById(req.params.id);

        if (!cliente){
            res.status(404).json({msg:'no se puede encontrar ese cliente'})
        }
        res.send(cliente);

    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");

    }
}
// Eliminar cliente

exports.eliminarCliente = async (req,res) =>{
    try{
        let cliente=await Cliente.findById(req.params.id);

        if (!cliente){
            res.status(404).json({msg:'no existe cliente'})
        }
        await Cliente.findOneAndRemove({_id: req.params.id });
        res.json({msg: 'el cliente está eliminado'});

    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");

    }
}
// ACTUALIZAR CLIENTE
exports.actualizarCliente = async (req,res) => {
    try{
        const {nombre, identificacion, ciudad, correo, telefono} =req.body;
        let cliente = await Cliente.findById(req.params.id);
        if (!cliente){
            res.status(404).json({msg: 'el cliente no existe'});

        }
        cliente.nombre= nombre;
        cliente.identificacion = identificacion;
        cliente.ciudad = ciudad;
        cliente.correo = correo;
        cliente.telefono = telefono;
        
        cliente = await Cliente.findOneAndUpdate({_id: req.params.id}, cliente,{new:true});
        res.json(cliente);
    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");
    }
}
