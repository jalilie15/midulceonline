const mongoose=require('mongoose');
//MODELO QUE DEBE SER IGUAL AL ESQUEMA DE LA BD
const proveedoresShema= mongoose.Schema({
    nombre:{
        type:String,
        required:true
    },
    tipo:{
        type:String,
        required:true
    },
    dia_pedido:{
        type:Number,
        required:true
    },
    valor_minimo_pedido:{
        type:Number,
        required:true
    },
    telefono:{
        type:Number,
        required:true
    },
    ciudad:{
        type:String,
        required:true
    },
    correo:{
        type:String,
        required:true
    }

},{ versionKey: false });

module.exports=mongoose.model("Proveedores", proveedoresShema);